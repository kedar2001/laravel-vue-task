import { createWebHistory, createRouter } from 'vue-router'
import Register from './component/Register.vue'
import LoginComponent from './component/LoginComponent.vue'
import ViewData from './component/ViewData.vue'
import Update from './component/Update.vue'
// Vue.use(Router)

const routes = [
        {
            path: '/register',
            name: 'Register',
            component: Register
        },
        {
            path: '/login',
            name: 'LoginComponent',
            component: LoginComponent
        },
        {
            path: '/view',
            name: 'ViewData',
            component: ViewData
        },
        {
            path: '/edit/:id',
            name: 'Update',
            component: Update
        }
    ]

const router = createRouter({
    history:createWebHistory(),
    routes,
})

export default router