<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserRegisterController extends Controller
{
    // public function __invoke(Request $request)
    // {
    //     return "Welcome to our homepage";
    // }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        echo "jereere"; die;
    }

    public function login(Request $request){
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $success['token'] = $user->createToken('Token')-> accessToken;
            $success['email'] = $user->email;

            // return $this->sendResponse($success, 'User Login Successfully');
            return response()->json([
                'success'=> $success,
                'state' => 200,
            ]);
        }
        else{
            // return $this->sebdError('Unauthorized', ['error'=>'Unauthorized']);
            return response()->json(['error'=> 'Unauthorized'],401);
        }
    }
    /**
     * Store a newly created resource in storage.
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            // // 'c_password' => 'required|same:password',
            'contact_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'birthdate' => 'required',
            // 'image' => 'required',
            'gender' => 'required',
            'country' => 'required',
            'state' => 'required',
            'address' => 'required',
        ]);
   
        if($validator->fails()){
            // return $this->sendError('Validation Error.', $validator->errors()); 
            return response()->json(['error'=> 'Error'],401);      
        }
   
        $input = $request->all();
        $input =new User();
        $input->name = $request['name'];
        $input->email = $request['email'];
        $input->password = Hash::make($request['password']);
        $input->contact_no = $request['contact_no'];
        $input->birthdate = $request['birthdate'];
        $input->image = $request['image'];
        $input->gender = $request['gender'];
        $input->country = $request['country'];
        $input->state = $request['state'];
        $input->address = $request['address'];

        $input->save();
        // $input['password'] = bcrypt($input['password']);
        // $user = User::create($input);
        $success['token'] =  $input->createToken('Token')->accessToken;
        $success['name'] =  $input->name;
   
        // return $this->sendResponse($success, 'User register successfully.');
        return response()->json(['success'=> $success]);
    }

    /**
     * Display the specified resource.
     */
    public function showuser(){
        return User::all();   
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            // // 'c_password' => 'required|same:password',
            'contact_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'birthdate' => 'required',
            // 'image' => 'required',
            'gender' => 'required',
            'country' => 'required',
            'state' => 'required',
            'address' => 'required',
        ]);
   
        if($validator->fails()){
            // return $this->sendError('Validation Error.', $validator->errors()); 
            return response()->json(['error'=> 'Error'],401);      
        }

        $input = User::find($id);
        $input-> name = $request['name'];
        $input-> email = $request['email'];
        $input->password = Hash::make($request['password']);
        $input->contact_no = $request['contact_no'];
        $input->birthdate = $request['birthdate'];
        $input-> gender = $request['image'];
        $input-> gender = $request['gender'];
        $input->country = $request['country'];
        $input->state = $request['state'];
        $input->address = $request['address'];
        // $filename = "profile" . time() . "." . $request->file('profile_image')->getClientOriginalExtension();
        // $request->file('profile_image')->storeAs('public/profile.image', $filename);
        // $input->profile_image = $filename;
        $input-> save();

        $success['token'] =  $input->createToken('Token')->accessToken;
        $success['name'] =  $input->name;
   
        // return $this->sendResponse($success, 'User register successfully.');
        return response()->json(['Updated'=> $success],200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(  $id)
    {
        $input = User::find($id);
        if (!is_null($input)){
            $input->delete();
        }
        return response()->json(['Deleted'=> $input],200);
    }

    public function searchData(Request $request)
    {
        $data = User::where('name', 'LIKE', '%'.$request->search.'%')->orWhere('email', 'LIKE', '%'.$request->search.'%')->get();
        return response()->json($data);

    }
}
