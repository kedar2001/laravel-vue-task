<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\UserRegisterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('/register', [UserRegisterController::class, 'register']);
Route::post('login', [UserRegisterController::class, 'login']);
Route::get('get-user', [UserRegisterController::class,'showuser']);
Route::post('/edit/{id}', [UserRegisterController::class, 'update']);
Route::delete('/delete/{id}', [UserRegisterController::class, 'destroy']);
Route::get('/search', [UserRegisterController::class, 'searchData']);

// Route::middleware('auth:api')->group(function(){
//     Route::get('get-user', [UserRegisterController::class,'getuser']);
// });